<?php

namespace ATM\InboxBundle\Queues\InboxSendAll;

use XLabs\RabbitMQBundle\RabbitMQ\Producer as Parent_Producer;

class Producer extends Parent_Producer
{
    public static function getQueueName()
    {
        // set your custom queue name for this producer here
        return 'bf_inbox_send_all';
    }

    // if you need to modify the data sent in the message, create the following function
    public function _process($data)
    {
        // here you can deal with the data in the message if needed before being consumed
    }
}