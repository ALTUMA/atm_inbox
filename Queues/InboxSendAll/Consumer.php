<?php

namespace ATM\InboxBundle\Queues\InboxSendAll;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use ATM\InboxBundle\Entity\Message;
use ATM\InboxBundle\Entity\Conversation;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'atm_inbox_send_all:execute';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $configuration = $container->getParameter('atm_inbox_config');
        $em = $container->get('doctrine.orm.default_entity_manager');


        $body = json_decode($msg->body,true);
        $user_id = $body['user_id'];
        $role = $body['role'] == 'ROLE_USER' ? 'a:0:{}' : $body['role'];
        $subject = $body['subject'];
        $body = $body['body'];


        $author = $em->getRepository($configuration['class']['model']['user'])->findOneById($user_id);
        $qbReceivers = $em->createQueryBuilder();

        $qbReceivers
            ->select('u')
            ->from($configuration['class']['model']['user'],'u')
            ->where(
                $qbReceivers->expr()->andX(
                    $qbReceivers->expr()->eq('u.enabled',1),
                    $qbReceivers->expr()->eq('u.locked',0),
                    $qbReceivers->expr()->eq('u.approved',1),
                    $qbReceivers->expr()->like('u.roles',$qbReceivers->expr()->literal('%'.$role.'%'))
                )
            );

        $receivers = $qbReceivers->getQuery()->getResult();
        dump($role);
        if(count($receivers) > 0){
            foreach($receivers as $receiver){
                if($user_id != $receiver->getId()){
                    dump($receiver->getUsername());
                    $message = new Message();
                    $message->setSubject($subject);
                    $message->setAuthor($author);
                    $message->setBody($body);
                    $message->setReceiver($receiver);

                    $conversation = new Conversation();
                    $conversation->setSentMassive(true);
                    $container->get('atm_inbox_manager')->sendMessage($author, $receiver, $conversation, $message);
                }
            }
            dump('send to all users');
        }
    }
}

