<?php

namespace ATM\InboxBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Event;


class CheckForSpamListener{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onCheckForSpam(Event $event){
        $message = $event->getMessage();
        $body = $message->getBody();

        $regularExpressions = array(
            '/(((http|https|ftp|ftps)\:\/\/)|(www\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\:[0-9]+)?(\/\S*)?/',
            '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i',
            '/onlyfans/'
        );

        foreach ($regularExpressions as $regexp){
            preg_match_all($regexp, $body, $matches);
            $matches = array_filter($matches);
            if(!empty($matches)){
                $message->setIsSpam(true);
                $this->em->persist($message);
                $this->em->flush();
            }
        }
    }
}