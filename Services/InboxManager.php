<?php

namespace ATM\InboxBundle\Services;

use Doctrine\ORM\EntityManager;
use ATM\InboxBundle\Entity\ConversationMessage;
use Doctrine\DBAL\Cache\QueryCacheProfile;

class InboxManager
{
    private $configuration;
    private $em;

    public function __construct(EntityManager $em,$configuration)
    {
        $this->em = $em;
        $this->configuration = $configuration;
    }

    public function sendMessage($author,$destinatary,$conversation, $message)
    {
        $this->em->persist($message);
        $this->em->flush();
        $this->em->persist($conversation);
        $this->em->flush();

        $message->setAuthor($author);
        $message->setReceiver($destinatary);

        $conversationMessageAuthor = new ConversationMessage();
        $conversationMessageReceiver = new ConversationMessage();

        $conversationMessageAuthor->setConversation($conversation);
        $conversationMessageAuthor->setMessage($message);
        $conversationMessageAuthor->setUser($author);
        $conversationMessageAuthor->setIsRead(true);
        $this->em->persist($conversationMessageAuthor);
        $this->em->flush();

        $conversationMessageReceiver->setConversation($conversation);
        $conversationMessageReceiver->setMessage($message);
        $conversationMessageReceiver->setUser($destinatary);
        $this->em->persist($conversationMessageReceiver);
        $this->em->flush();

        return array(
            'cvAuthor' => $conversationMessageAuthor,
            'cvReceiver' => $conversationMessageReceiver
        );
    }

    public function getReceivedConversations($userId,$onlyIds = false,$conversationsIds = null){
        $qb = $this->em->createQueryBuilder();

        if($onlyIds){
            $qb->select('c')
                ->addSelect('cm')
                ->addSelect('cm_m');

        }else{
            $qb
                ->select('c')
                ->addSelect('cm')
                ->addSelect('cm_m')
                ->addSelect('cm_m_r')
                ->addSelect('cm_m_a')
                ->groupBy('c');
        }

        $qb
            ->from('ATMInboxBundle:Conversation','c')
            ->join('c.conversationMessages','cm')
            ->join('cm.user','cm_u','WITH',$qb->expr()->eq('cm_u',$userId))
            ->join('cm.message','cm_m')
            ->join('cm_m.receiver','cm_m_r','WITH',$qb->expr()->eq('cm_m_r.id',$userId))
            ->join('cm_m.author','cm_m_a')
            ->addOrderBy('cm.isRead','ASC')
            ->addOrderBy('cm_m.creationdate','DESC');


        if($conversationsIds){
            $qb->where(
                $qb->expr()->in('c.id',$conversationsIds)
            );
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getSentConversations($userId,$onlyIds = false,$conversationsIds = null, $sentMassive = false){
        $qb = $this->em->createQueryBuilder();

        if($onlyIds){
            $qb->select('c')
                ->addSelect('cm')
                ->addSelect('cm_m');
        }else{
            $qb
                ->select('c')
                ->addSelect('cm')
                ->addSelect('cm_m')
                ->addSelect('cm_m_r')
                ->addSelect('cm_m_a')
                ->groupBy('c');
        }

        $qb
            ->from('ATMInboxBundle:Conversation','c')
            ->join('c.conversationMessages','cm')
            ->join('cm.user','cm_u','WITH',$qb->expr()->eq('cm_u',$userId))
            ->join('cm.message','cm_m')
            ->join('cm_m.receiver','cm_m_r')
            ->join('cm_m.author','cm_m_a','WITH',$qb->expr()->eq('cm_m_a.id',$userId))
            ->orderBy('cm.isRead','ASC')
            ->addOrderBy('cm_m.creationdate','DESC');


        if($conversationsIds){
            $qb->where(
                $qb->expr()->in('c.id',$conversationsIds)
            );
        }

        if($sentMassive){
            $qb->andWhere(
                $qb->expr()->eq('c.sent_massive',1)
            );
        }else{
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->eq('c.sent_massive',0),
                    $qb->expr()->isNull('c.sent_massive')
                )
            );
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getConversationMessages($conversationId,$userId,$arrayResult = true){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('c')
            ->addSelect('cm')
            ->addSelect('cmu')
            ->addSelect('m')
            ->addSelect('a')
            ->addSelect('r')
            ->from('ATMInboxBundle:Conversation','c')
            ->join('c.conversationMessages','cm')
            ->join('cm.message','m')
            ->join('m.author','a')
            ->join('m.receiver','r')
            ->where($qb->expr()->eq('c.id',$conversationId))
            ->orderBy('m.creationdate','ASC');

        if($userId){
            $qb->join('cm.user','cmu','WITH',$qb->expr()->eq('cmu.id',$userId));
        }else{
            $qb->join('cm.user','cmu');
        }

        // When multiple joins are found in query, and associations can becoma large (like tags, actors, etc), the hydration takes long to get the array resultset; besides caching the result, also cache hydration result and PROBLEM RESOLVED!
        $cache = $this->em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(ConversationMessage::RESULT_CACHE_CONVERSATION_ITEM_TTL, ConversationMessage::RESULT_CACHE_CONVERSATION_ITEM.$conversationId.'_hydration', $cache);

        $query = $qb->getQuery();

        $query
            ->useQueryCache(true)
            ->setResultCacheLifetime(ConversationMessage::RESULT_CACHE_CONVERSATION_ITEM_TTL)
            ->setResultCacheId(ConversationMessage::RESULT_CACHE_CONVERSATION_ITEM.$conversationId)
            ->setHydrationCacheProfile($hydrationCacheProfile);


        $result = ($arrayResult) ? $query->getArrayResult() : $query->getResult();

        return $result[0];
    }

    public function getConversationById($conversationId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial c.{id}')
            ->from('ATMInboxBundle:Conversation','c')
            ->where($qb->expr()->eq('c.id',$conversationId));

        $result =  $qb->getQuery()->getResult();

        return $result[0];
    }

    public function getByConversationIdAndUserId($conversationId,$userId, $arrayResults = true ){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('cm')
            ->addSelect('m')
            ->addSelect('a')
            ->addSelect('r')
            ->from('ATMInboxBundle:ConversationMessage','cm')
            ->join('cm.conversation','c','WITH',$qb->expr()->eq('c.id',$conversationId))
            ->join('cm.user','cu','WITH',$qb->expr()->eq('cu.id',$userId))
            ->join('cm.message','m')
            ->join('m.author','a')
            ->join('m.receiver','r')
            ->orderBy('cm.id','ASC');

        if($arrayResults){
            return $qb->getQuery()->getArrayResult();
        }else{
            return $qb->getQuery()->getResult();
        }
    }

    public function totalUnreadMessagesByUserId($userId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('COUNT(cm.id)')
            ->from('ATMInboxBundle:ConversationMessage','cm')
            ->join('cm.user','cmu','WITH',$qb->expr()->eq('cmu.id',$userId))
            ->join('cm.message','cmm')
            ->join('cmm.receiver','cmr','WITH',$qb->expr()->eq('cmr.id',$userId))
            ->where($qb->expr()->eq('cm.isRead',0));

        return $qb->getQuery()->getArrayResult();
    }

    public function getMessageById($messageId,$userId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('m')
            ->addSelect('a')
            ->addSelect('r')
            ->addSelect('cm')
            ->addSelect('cm_conversation')
            ->from('ATMInboxBundle:Message','m')
            ->join('m.author','a')
            ->join('m.receiver','r')
            ->join('m.conversationMessages','cm')
            ->join('cm.user','cmconv_user','WITH',$qb->expr()->eq('cmconv_user.id',$userId))
            ->join('cm.conversation','cm_conversation')
            ->where($qb->expr()->eq('m.id',$messageId));

        $result = $qb->getQuery()->getArrayResult();

        return $result[0];
    }

    public function searchUsers($searchText){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('u')
            ->from($this->configuration['class']['model']['user'],'u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('u.username',$qb->expr()->literal('%'.$searchText.'%'),
                    $qb->expr()->eq('u.enabled',1),
                        $qb->expr()->eq('u.locked',0)
                )
            ));

        return $qb->getQuery()->getArrayResult();
    }

    public function getUserByCanonical($usernameCanonical){

        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('u')
            ->from($this->configuration['class']['model']['user'],'u')
            ->where(
                $qb->expr()->eq('u.usernameCanonical',$qb->expr()->literal($usernameCanonical))
            );

        return $qb->getQuery()->getArrayResult();
    }
}