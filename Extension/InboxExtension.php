<?php

namespace ATM\InboxBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ATM\InboxBundle\Services\InboxManager;

class InboxExtension extends \Twig_Extension{

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('getUnreadMessages', array($this, 'getUnreadMessages')),
        );
    }

    public function getUnreadMessages($userId){
        $inboxManager = $this->container->get(InboxManager::class);


        $unreadMessages = $inboxManager->totalUnreadMessagesByUserId($userId);

        return $unreadMessages[0][1];
    }
}