<?php

namespace ATM\InboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_custom_message")
 */
class CustomMessage{
    const NEW_USER = 'new_user';
    const NEW_MODEL = 'new_model';
    const MODEL_MAX_POINTS_REACHED = 'model_max_points_reached';


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $subject;

    /**
     * @ORM\Column(name="body", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $body;

    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}