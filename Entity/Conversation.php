<?php

namespace ATM\InboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="conversation")
 */
class Conversation{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    protected $creationdate;

    /**
     * @ORM\OneToMany(targetEntity="ConversationMessage", mappedBy="conversation")
    */
    protected $conversationMessages;

    /**
     * @ORM\Column(name="sent_massive", type="boolean", nullable=true)
     */
    private $sent_massive;


    public function __construct(){
        $this->conversationMessages = new ArrayCollection();
        $this->creationdate = new DateTime();
        $this->sent_massive = false;
    }


    public function getCreationdate(){
        return $this->creationdate;
    }

    public function setCreationdate($creationdate){
        $this->creationdate = $creationdate;
    }

    public function getConversationMessages()
    {
        return $this->conversationMessages;
    }

    public function addConversationMessage($conversationMessage)
    {
        $this->conversationMessages->add($conversationMessage);
    }

    public function removeConversationMessage($conversationMessage){
        $this->conversationMessages->removeElement($conversationMessage);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSentMassive()
    {
        return $this->sent_massive;
    }

    public function setSentMassive($sent_massive)
    {
        $this->sent_massive = $sent_massive;
    }
}