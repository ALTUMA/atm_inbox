<?php

namespace ATM\InboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="message")
 */
class Message{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $subject;

    /**
     * @ORM\Column(name="body", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $body;

    protected $receiver;

    protected $author;

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    protected $creationdate;

    /**
     * @ORM\OneToMany(targetEntity="ConversationMessage", mappedBy="message")
     */
    protected $conversationMessages;

    /**
     * @ORM\Column(name="is_spam", type="boolean", nullable=true)
     */
    private $isSpam;

    /**
     * @ORM\Column(name="spam_checked", type="boolean", nullable=true)
     */
    private $spamChecked;

    public function __construct()
    {
        $this->conversationMessages = new ArrayCollection();
        $this->creationdate = new DateTime();
        $this->isSpam = false;
        $this->spamChecked = false;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getReceiver()
    {
        return $this->receiver;
    }

    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getConversationMessages()
    {
        return $this->conversationMessages;
    }

    public function addConversationMessage($conversationMessage)
    {
        $this->conversationMessages->add($conversationMessage);
    }

    public function removeConversationMessage($conversationMessage){
        $this->conversationMessages->removeElement($conversationMessage);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIsSpam()
    {
        return $this->isSpam;
    }

    public function setIsSpam($isSpam)
    {
        $this->isSpam = $isSpam;
    }

    public function getSpamChecked()
    {
        return $this->spamChecked;
    }

    public function setSpamChecked($spamChecked)
    {
        $this->spamChecked = $spamChecked;
    }
}