<?php

namespace ATM\InboxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FroalaController extends Controller{

    public function uploadImageAction($folder){
        $config = $this->getParameter('atm_inbox_config');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $request = $this->get('request_stack')->getCurrentRequest();

        $userFolder = $this->get('kernel')->getRootDir().'/../web/media/'.$config['froala']['media_folder'].'/'.$user->getFolder();
        if(!is_dir($userFolder)){
            mkdir($userFolder);
        }

        $messageImagesFolder = $userFolder.'/'.$folder;
        if(!is_dir($messageImagesFolder)){
            mkdir($messageImagesFolder);
        }

        $file = $request->files->get('froala_image_file');
        $imageName = $file->getClientOriginalName();
        $file->move($messageImagesFolder,$imageName);

        return new Response(json_encode(array('link'=>'/media/'.$config['froala']['media_folder'].'/'.$user->getFolder().'/'.$folder.'/'.$imageName)));
    }

    public function deleteImageAction($path){
        $imageFolder = $this->get('kernel')->getRootDir().'/../web'.urldecode($path);
        unlink($imageFolder);
        return new Response(json_encode('success'));
    }

}