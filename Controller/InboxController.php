<?php

namespace ATM\InboxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\InboxBundle\Form\MessageType;
use ATM\InboxBundle\Entity\Message;
use ATM\InboxBundle\Queues\InboxSendAll\Producer;
use ATM\InboxBundle\Entity\Conversation;
use ATM\InboxBundle\Entity\CustomMessage;
use ATM\InboxBundle\Event\InboxSendToAllEvent;
use ATM\InboxBundle\Services\InboxManager;
use ATM\InboxBundle\Event\CheckForSpam;

class InboxController extends Controller
{
    public function inboxAction($page){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $inboxManager = $this->get(InboxManager::class);
        $receivedConversations = $inboxManager->getReceivedConversations($user->getId(),true);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $receivedConversations,
            $page,
            20
        );

        $arrConversationsIds = array();
        foreach($pagination->getItems() as $item){
            $arrConversationsIds[] = $item['id'];
        }



        $conversations = array();
        if(count($arrConversationsIds) > 0){
            foreach($arrConversationsIds as $id){
                $conversation = $inboxManager->getConversationMessages($id,$user->getId());
                $conversation['isRead'] = true;
                foreach($conversation['conversationMessages'] as $convMessage){
                    if($convMessage['message']['author']['id'] != $user->getId()){
                        $conversation['author'] = $convMessage['message']['author'];
                    }

                    if(!$convMessage['isRead']){
                        $conversation['isRead'] = false;
                    }
                    $conversation['message_id'] = $convMessage['message']['id'];
                }
                $conversation['subject'] = $conversation['conversationMessages'][0]['message']['subject'];
                $conversation['creationdate'] = $conversation['conversationMessages'][0]['message']['creationdate'];

                $conversations[] = $conversation;
            }
        }

        return $this->render('ATMInboxBundle:Inbox:receivedMessages.html.twig',array(
            'receivedConversations' => $conversations,
            'pagination' => $pagination,
            'page' => $page
        ));
    }

    public function sentMessagesAction($page){
        $inboxManager = $this->get(InboxManager::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $sentConversationsIds = $inboxManager->getSentConversations($user->getId(),true);


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sentConversationsIds,
            $page,
            20
        );

        $arrConversationsIds = array();
        foreach($pagination->getItems() as $item){
            $arrConversationsIds[] = $item['id'];
        }

        $conversations = array();
        if(count($arrConversationsIds) > 0){
            foreach($arrConversationsIds as $id){
                $conversation = $inboxManager->getConversationMessages($id,$user->getId());
                foreach($conversation['conversationMessages'] as $convMessage){
                    if($convMessage['message']['receiver']['id'] != $user->getId()){
                        $conversation['author'] = $convMessage['message']['receiver'];
                        break;
                    }
                }
                $conversation['message_id'] = $conversation['conversationMessages'][0]['message']['id'];
                $conversation['subject'] = $conversation['conversationMessages'][0]['message']['subject'];
                $conversation['creationdate'] = $conversation['conversationMessages'][0]['message']['creationdate'];
                $conversation['isRead'] = true;

                $conversations[] = $conversation;
            }
        }

        return $this->render('ATMInboxBundle:Inbox:sentMessages.html.twig',array(
            'sentConversations' => $conversations,
            'pagination' => $pagination,
            'page' => $page
        ));
    }

    public function createMessageAction($usernameCanonical)
    {
        $configuration = $this->getParameter('atm_inbox_config');
        $em = $this->getDoctrine()->getManager();
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);
        $request = $this->get('request_stack')->getCurrentRequest();

        if($request->getMethod() == 'POST')
        {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $form->handleRequest($request);

            $receiverId = $request->get('userId');
            if($receiverId) {
                $receiver = $em->getRepository($configuration['class']['model']['user'])->findOneById($receiverId);
                $conversation = new Conversation();
                $this->get(InboxManager::class)->sendMessage($user, $receiver, $conversation, $message);

                $event = new CheckForSpam($message);
                $this->get('event_dispatcher')->dispatch(CheckForSpam::NAME, $event);
            }else{
                $sendAll = $request->get('sendAll');
                foreach($sendAll as $key => $value){
                    $producer = $this->get('inbox_send_all_producer');
                    $producer->process(
                        array(
                            'user_id' => $user->getId(),
                            'role'=>$value,
                            'body' => $message->getBody(),
                            'subject' => $message->getSubject()
                        )
                    );

                    $this->get('session')->getFlashBag()->set('massive_message_queued','message queued succesfully');
                }
            }

            return new RedirectResponse($this->get('router')->generate('inbox_index'));
        }

        $user = null;
        if($usernameCanonical){
            $inboxManager = $this->get(InboxManager::class);
            $user = $inboxManager->getUserByCanonical($usernameCanonical)[0];
        }

        return $this->render('ATMInboxBundle:Inbox:createMessage.html.twig',array(
            'form' => $form->createView(),
            'user' => $user,
            'froala_key' => $configuration['froala']['key'],
            'sendToAllRoles' => $configuration['send_to_all_roles']
        ));
    }



    public function searchUsersAction($searchTerm){
        $request = $this->get('request_stack')->getCurrentRequest();
        $inboxManager = $this->get(InboxManager::class);

        if($request->isXmlHttpRequest()){
            $userSession = $this->get('security.token_storage')->getToken()->getUser();
            $users = $inboxManager->searchUsers($searchTerm);

            $usersHtml = '';
            foreach($users as $user){
                if($user['id'] != $userSession->getId()){
                    $usersHtml .= $this->renderView('ATMInboxBundle:Inbox:item/userItem.html.twig',array(
                        'user' => $user
                    ));
                }
            }

            return new Response($usersHtml);
        }
    }

    public function seeMessageBodyAction($messageId){
        $inboxManager = $this->get(InboxManager::class);
        $configuration = $this->getParameter('atm_inbox_config');
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $message = $inboxManager->getMessageById($messageId,$user->getId());

        //SET ALL CONVERSATION MESSAGES AS READ
        $conversationId = $message['conversationMessages'][0]['conversation']['id'];
        $conversation = $inboxManager->getConversationMessages($conversationId,$user->getId(),false);

        foreach($conversation->getConversationMessages() as $cMessage){
            if(!$cMessage->getIsRead()){
                $cMessage->setIsRead(true);
                $em->persist($cMessage);
            }
        }
        $em->flush();


        //GET ALL THE MESSAGES FOR THAT USER FOR THAT CONVERSATION
        $conversationId = $message['conversationMessages'][0]['conversation']['id'];
        $conversationMessages = $inboxManager->getByConversationIdAndUserId($conversationId,$user->getId());

        //SET THE RECEIVER OF THE REPLY FORM
        $receiver = null;
        foreach($conversationMessages as $cvMessage){
            if($cvMessage['message']['receiver']['id'] != $user->getId()){
                $receiver = $cvMessage['message']['receiver'];
                break;
            }
        }

        if(is_null($receiver)){
            $receiver = $message['author'];
        }

        $newMessage = new Message();
        $form = $this->createForm(MessageType::class, $newMessage);

        return $this->render('ATMInboxBundle:Inbox:readMessage.html.twig',array(
            'message' => $message,
            'conversationMessages'=>$conversationMessages,
            'form' => $form->createView(),
            'conversationId' => $conversationId,
            'receiver' => $receiver,
            'froala_key' => $configuration['froala']['key']
        ));
    }

    public function replyMessageAction(){
        $configuration = $this->getParameter('atm_inbox_config');
        $inboxManager = $this->get(InboxManager::class);
        $em = $this->getDoctrine()->getManager();
        $newMessage = new Message();
        $form = $this->createForm(MessageType::class, $newMessage);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            $receiverId = $request->get('userId');
            $receiver = $em->getRepository($configuration['class']['model']['user'])->findOneById($receiverId);

            $conversationId = $request->get('conversationId');
            $conversation = $inboxManager->getConversationById($conversationId);

            $this->get('atm_inbox_manager')->sendMessage($user, $receiver, $conversation, $newMessage);

            $event = new CheckForSpam($newMessage);
            $this->get('event_dispatcher')->dispatch(CheckForSpam::NAME, $event);

            return new RedirectResponse($this->get('router')->generate('inbox_index'));
        }
    }

    public function markMessagesAsReadAction($ids){
        $inboxManager = $this->get(InboxManager::class);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->isXmlHttpRequest()){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();
            $conversationIds = explode(',',$ids);

            foreach($conversationIds as $conversationId){
                $conversationMessages = $inboxManager->getByConversationIdAndUserId($conversationId,$user->getId(), false);

                foreach($conversationMessages as $conversationMessage){
                    if(!$conversationMessage->getIsRead()){
                        $conversationMessage->setIsRead(true);
                        $em->persist($conversationMessage);
                    }
                }
            }
            $em->flush();

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $unreadMessages = $inboxManager->totalUnreadMessagesByUserId($user->getId());
            return new Response(json_encode(array('unreadMessages'=>$unreadMessages[0][1])));
        }
    }

    public function markMessagesAsUnreadAction($ids){

        $inboxManager = $this->get(InboxManager::class);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->isXmlHttpRequest()){
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();
            $conversationIds = explode(',',$ids);

            foreach($conversationIds as $conversationId){
                $conversationMessages = $inboxManager->getByConversationIdAndUserId($conversationId,$user->getId(), false);

                foreach($conversationMessages as $conversationMessage){
                    if($conversationMessage->getIsRead()){
                        $conversationMessage->setIsRead(false);
                        $em->persist($conversationMessage);
                    }
                }
            }
            $em->flush();

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $unreadMessages = $inboxManager->totalUnreadMessagesByUserId($user->getId());
            return new Response(json_encode(array('unreadMessages'=>$unreadMessages[0][1])));
        }
    }


    public function deleteMessagesAction($ids){
        $request = $this->get('request_stack')->getCurrentRequest();
        $inboxManager = $this->get(InboxManager::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $conversationsIds = explode(',',$ids);

        $em = $this->getDoctrine()->getManager();
        foreach($conversationsIds as $conversationId){
            $conversationMessages = $inboxManager->getByConversationIdAndUserId($conversationId,$user->getId(), false);
            foreach($conversationMessages as $conversationMessage){
                $em->remove($conversationMessage);
            }
        }

        $em->flush();

        if($request->isXmlHttpRequest()){
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $unreadMessages = $inboxManager->totalUnreadMessagesByUserId($user->getId());
            return new Response(json_encode(array('unreadMessages'=>$unreadMessages[0][1])));
        }else{
            $request->getSession()->getFlashBag()->add('messageDeletedSuccesfully','Message deleted succesfully');
            return new RedirectResponse($this->get('router')->generate('inbox_index'));
        }
    }

    public function redrawMessagesTableAction($inboxType,$page){
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $inboxManager = $this->get(InboxManager::class);

        if($inboxType == 'received'){
            $dbConversations = $inboxManager->getReceivedConversations($user->getId(),true);
        }elseif($inboxType == 'sent'){
            $dbConversations = $inboxManager->getSentConversations($user->getId(),true);
        }

        if(count($dbConversations) > 0){
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $dbConversations,
                $page,
                20
            );

            $arrConversationsIds = array();
            foreach($pagination->getItems() as $item){
                $arrConversationsIds[] = $item['id'];
            }

            if($inboxType == 'received') {
                $conversations = array();
                foreach($arrConversationsIds as $id){
                    $conversation = $inboxManager->getConversationMessages($id,$user->getId());
                    foreach($conversation['conversationMessages'] as $convMessage){
                        if($convMessage['message']['author']['id'] != $user->getId()){
                            $conversation['author'] = $convMessage['message']['author'];
                            break;
                        }
                        $conversation['message_id'] = $convMessage['message']['id'];
                    }
                    $conversation['subject'] = $conversation['conversationMessages'][0]['message']['subject'];
                    $conversation['creationdate'] = $conversation['conversationMessages'][0]['message']['creationdate'];
                    $conversation['isRead'] = $conversation['conversationMessages'][0]['isRead'];
                    $conversation['message_id'] = $conversation['conversationMessages'][0]['message']['id'];
                    $conversations[] = $conversation;
                }

            }elseif($inboxType == 'sent'){
                $conversations = array();
                if(count($arrConversationsIds) > 0){
                    foreach($arrConversationsIds as $id){
                        $conversation = $inboxManager->getConversationMessages($id,$user->getId());
                        foreach($conversation['conversationMessages'] as $convMessage){
                            if($convMessage['message']['receiver']['id'] != $user->getId()){
                                $conversation['author'] = $convMessage['message']['receiver'];
                                break;
                            }
                        }
                        $conversation['subject'] = $conversation['conversationMessages'][0]['message']['subject'];
                        $conversation['creationdate'] = $conversation['conversationMessages'][0]['message']['creationdate'];
                        $conversation['message_id'] = $conversation['conversationMessages'][0]['message']['id'];
                        $conversation['isRead'] = $conversation['conversationMessages'][0]['isRead'];

                        $conversations[] = $conversation;
                    }
                }
            }

            $html = '';
            foreach($conversations as $conversation){
                $html .= $this->renderView('ATMInboxBundle:Inbox:item/conversation.html.twig',array(
                    'conversation' => $conversation
                ));
            }

            return new Response($html);
        }else{
            return new Response('<p class="noMsg">No messages to show</p>');
        }
    }

    public function sentMassiveMessagesAction($page){

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $inboxManager = $this->get(InboxManager::class);
        $sentConversationsIds = $inboxManager->getSentConversations($user->getId(),false,null,true);


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sentConversationsIds,
            $page,
            20
        );

        $arrConversationsIds = array();
        foreach($pagination->getItems() as $item){
            $arrConversationsIds[] = $item['id'];
        }

        $conversations = array();
        if(count($arrConversationsIds) > 0){
            foreach($arrConversationsIds as $id){
                $conversation = $inboxManager->getConversationMessages($id,$user->getId());
                foreach($conversation['conversationMessages'] as $convMessage){
                    if($convMessage['message']['receiver']['id'] != $user->getId()){
                        $conversation['author'] = $convMessage['message']['receiver'];
                    }
                    $conversation['message_id'] = $convMessage['message']['id'];
                    break;
                }
                $conversation['subject'] = $conversation['conversationMessages'][0]['message']['subject'];
                $conversation['creationdate'] = $conversation['conversationMessages'][0]['message']['creationdate'];
                $conversation['isRead'] = true;

                $conversations[] = $conversation;
            }
        }

        return $this->render('ATMInboxBundle:Inbox:sentMassiveMessages.html.twig',array(
            'sentConversations' => $conversations,
            'pagination' => $pagination,
            'page' => $page
        ));

    }

    public function customDMsAction($type){
        $configuration = $this->getParameter('atm_inbox_config');
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $customMessage = $em->getRepository('ATMInboxBundle:CustomMessage')->findOneByType($type);

        if($request->getMethod() == 'POST'){
            $subject = $request->get('subject');
            $body = $request->get('body');

            if(is_null($customMessage)){
                $customMessage = new CustomMessage();
            }

            $customMessage->setType($type);
            $customMessage->setSubject($subject);
            $customMessage->setBody($body);
            $em->persist($customMessage);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notAllowedAction','Message Saved Successfully');
        }


        return $this->render('ATMInboxBundle:CustomMessage:index.html.twig',array(
            'custom_message' => $customMessage,
            'froala_key' => $configuration['froala']['key'],
            'type' => $type
        ));
    }

    public function spamDMsAction(){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('m')
            ->addSelect('a')
            ->addSelect('r')
            ->from('ATMInboxBundle:Message','m')
            ->join('m.author','a')
            ->join('m.receiver','r')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('m.isSpam',1),
                    $qb->expr()->eq('m.spamChecked',0)
                )
            );

        $messages = $qb->getQuery()->getArrayResult();

        return $this->render('ATMInboxBundle:Spam:index.html.twig',array(
            'messages' => $messages
        ));
    }

    public function markCheckedSpamAction($messageId){
        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository('ATMInboxBundle:Message')->findOneById($messageId);
        $message->setSpamChecked(true);
        $em->persist($message);
        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_inbox_spam_dms'));
    }
}
