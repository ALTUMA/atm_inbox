<?php

namespace ATM\InboxBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('atm_inbox')
                        ->children()
                            ->arrayNode('class')->isRequired()
                                ->children()
                                    ->arrayNode('model')->isRequired()
                                        ->children()
                                            ->scalarNode('user')->isRequired()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('send_to_all_roles')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('name')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('froala')->isRequired()
                                ->children()
                                    ->scalarNode('key')->isRequired()->end()
                                    ->scalarNode('media_folder')->isRequired()->end()
                                ->end()
                            ->end()
                    ->end();

        return $treeBuilder;
    }
}
